# HattyGame
Here is the new and exciting HattyGame  for you. Help Hatty collect all letters in time!
Although, please keep in mind, this is still a protoype and it would be nice to leave some suggestions.

### Gameplay
Press `LEFT_KEY` or `RIGHT_KEY`to move Hatty.  Also Hatty can jump with `UP_KEY`.
Try to collect all letters in time - although it is not implemented as of yet due to this being a prototype.
## Compilation
Dependencies: 
GNU Compiler Collection or equivalent C compiler
#### On Windows
Download the binary listed on Releases or execute the following command:
>make install   
#### On Linux
Execute the following command:
>make install
